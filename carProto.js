/**
 * Car class
 * @constructor
 * @param {String} model
 */

class Car {
    constructor(model) {
        this.model = model;
        this.currentSpeed = 0;
    }

    accelerate() {
        this.currentSpeed++;
    }

    brake() {
        this.currentSpeed--;
    }

    toString() {
        return `${this.model} is moving at ${this.currentSpeed} MPH`;
    }
}

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
const honda = new Car('Honda');
honda.accelerate();
honda.accelerate();
honda.brake();
console.log(honda.toString());

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

 class ElectricCar extends Car {
     constructor(model) {
         super(model);
         this.motor = 'electric';
     }

     accelerate() {
         super.accelerate();
         super.accelerate();
     }

     toString() {
         return `${this.model} is moving at ${this.currentSpeed}`;
     }
 }

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
const tesla = new ElectricCar('Tesla');
tesla.accelerate();
tesla.accelerate();
tesla.brake();
console.log(tesla.toString());