
const form = document.getElementById('connect-form');

form.addEventListener('submit', function(e) {
    const firstName = document.getElementById('first-name');
    const lastName = document.getElementById('last-name');
    
    if (firstName.value.length < 5) {
        e.preventDefault();
        firstName.classList.add('invalid');
    } else {
        firstName.classList.remove('invalid');
    }

    if (lastName.value.length < 5) {
        e.preventDefault();
        lastName.classList.add('invalid');
    } else {
        lastName.classList.remove('invalid');
    }

});
