// TODO
const selectEl = document.getElementById('contact-kind');

const setSelectValid = function () {
    if (selectEl.value === 'choose') {
        selectEl.setCustomValidity('Must select an option');
        return;
    }
    selectEl.setCustomValidity('');

    businessEl = document.querySelector('.business');
    techEl = document.querySelector('.technical');

    if (selectEl.value === 'business') {
        businessEl.classList.remove('hide');
        businessEl.querySelector('input').required = true;
        techEl.classList.add('hide');
        techEl.querySelector('input').required = false;
    } else {
        businessEl.classList.add('hide');
        businessEl.querySelector('input').required = false;
        techEl.classList.remove('hide');
        techEl.querySelector('input').required = true;
    }
    
};

setSelectValid();

selectEl.addEventListener('change', setSelectValid);

const form = document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
    const inputs = document.getElementsByClassName('validate-input');
    let formIsValid = true;

    Array.from(inputs).forEach(input => {
        const small = input.parentElement.querySelector('small');

        if (!input.checkValidity()) {
            formIsValid = false;
            small.innerText = input.validationMessage;
        } else {
            small.innerText = '';
        }

    });

    if (!formIsValid) {
        e.preventDefault();
    }

});



