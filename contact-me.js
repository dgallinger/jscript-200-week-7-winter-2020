// get contact-kind drop down list
const selectEl = document.getElementById('contact-kind');

// function to flag needed input and toggle options based on selections
const setSelectValid = function () {
    if (selectEl.value === 'choose') {
        selectEl.setCustomValidity('Please select an option');
        return;
    }
    selectEl.setCustomValidity('');

    const jobOpp = document.querySelectorAll('.job');
    const talkCode = document.querySelector('.talk');

    if (selectEl.value === 'job') {
        jobOpp.forEach(node => {
            node.classList.remove('hide');
            node.querySelector('input').required = true;
        });
        
        talkCode.classList.add('hide');
        talkCode.querySelector('input').required = false;
    } else {
        jobOpp.forEach(node => {
            node.classList.add('hide');
            node.querySelector('input').required = false;
        });
        talkCode.classList.remove('hide');
        talkCode.querySelector('input').required = true;
    }
    
};

setSelectValid();

selectEl.addEventListener('change', setSelectValid);

const form = document.getElementById('connect-form');
form.addEventListener('submit', function(e) {
    const inputs = document.getElementsByClassName('validate-input');
    let formIsValid = true;

    Array.from(inputs).forEach(input => {
        const small = input.parentElement.querySelector('small');

        if (!input.checkValidity()) {
            formIsValid = false;
            small.innerText = input.validationMessage;
        } else {
            small.innerText = '';
        }

    });

    if (!formIsValid) {
        e.preventDefault();
    }

});